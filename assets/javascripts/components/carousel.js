import Swiper from 'swiper';
import { Navigation, Pagination } from 'swiper/modules';

export default function setCarousel() {
  const productsCarousel = document.querySelectorAll('[data-products-carousel]');

  productsCarousel.forEach((section) => {
    const carousel = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');
    const next = section.querySelector('.swiper-button-next');
    const prev = section.querySelector('.swiper-button-prev');

    if (carousel) {
      const swiper = new Swiper(carousel, {
        modules: [Navigation, Pagination],
        slidesPerView: 2,
        spaceBetween: 8,
        watchOverflow: true,
        speed: 1000,
        breakpoints: {
          768: {
            slidesPerView: 3,
            spaceBetween: 24,
            navigation: {
              nextEl: next,
              prevEl: prev,
            },
          },
          1440: {
            slidesPerView: 4,
            spaceBetween: 24,
            navigation: {
              nextEl: next,
              prevEl: prev,
            },
          },
        },
        pagination: {
          el: pagination,
          type: 'bullets',
          clickable: true,
        },
      });
    }
  });
}
