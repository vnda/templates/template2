import setTopBar from '../components/topBar';
const Login = {

  init: function () {
    setTopBar();
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Login.init()
})
