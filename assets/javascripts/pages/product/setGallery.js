import Swiper from 'swiper';
import { Manipulation, Navigation, Pagination, Zoom, Thumbs } from 'swiper/modules'

export default function setGallery(Product) {
  const thumbs = new Swiper(Product.thumbsSlider.element, {
    modules: [Manipulation],
    direction: 'horizontal',
    slidesPerView: 7,
    spaceBetween: 8,
    speed: 1000,
    watchSlidesProgress: true,
    watchOverflow: true,
    preloadImages: false,
    lazy: {
      checkInView: true,
      loadOnTransitionStart: true,
      loadPrevNext: false
    }
  });

  const main = new Swiper(Product.mainSlider.element, {
    modules: [Manipulation, Navigation, Zoom, Thumbs],
    slidesPerView: 1,
    speed: 1000,
    watchOverflow: true,
    watchSlidesProgress: true,
    thumbs: {
      swiper: thumbs,
    },
    navigation: {
      nextEl: '[data-main-slider] .swiper-button-next',
      prevEl: '[data-main-slider] .swiper-button-prev',
    },
    pagination: {
      el: '[data-product-images] .swiper-pagination',
      clickable: true,
      type: 'bullets'
    },
    preloadImages: false,
    lazy: {
      checkInView: true,
      loadOnTransitionStart: true
    },
    zoom: {
      maxRatio: 1.5,
      zoomedSlideClass: '-zoomed'
    }
  });

  Product.thumbsSlider.swiper = thumbs;
  Product.mainSlider.swiper = main;
}
