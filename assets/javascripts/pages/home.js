import setFullbanner from './home/1_fullbanner';
import setBannersLinha from './home/home_banner_linha';
import setCarousel from '../components/carousel';
import setBrandsCarousel from './home/brands_carousel';
import setBlogPosts from './home/2_2_categories';

import setIconsVertical from './home/icons_vertical';
//addImports

import setTopBar from '../components/topBar';
import handleConditionalLazy from '../components/conditionalLazy';

const Home = {
  init: function () {
    setTopBar();
    handleConditionalLazy();

    setFullbanner()
		setBannersLinha();
		setCarousel();
		setBrandsCarousel();
		setBlogPosts();
		
		setIconsVertical();
		//calls
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Home.init();
});
