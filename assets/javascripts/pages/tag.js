import { FilterComponent } from './tag/filters';
import InfinityScroll from '../components/infinityScroll';
import { updatePriceBlock } from '../components/utilities.js';
import setTopBar from '../components/topBar.js';
import setCategoriesSlider from './tag/categories_slider.js';

const Tag = {
  init: function () {
    setTopBar();
    setCategoriesSlider();
    FilterComponent.init();
    updatePriceBlock();
    InfinityScroll.init();
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Tag.init();
});
