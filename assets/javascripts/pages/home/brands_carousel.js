import Swiper from 'swiper';
import { Navigation, Pagination, Autoplay } from 'swiper/modules'

export default function setBrandsCarousel() {
  const brandsContainer = document.querySelector("[data-swiper-brands]");

  if (brandsContainer) {
    const brandsSwiper = new Swiper(brandsContainer.querySelector('.swiper'), {
      modules: [Autoplay, Navigation, Pagination],
      slidesPerView: 6,
      slidesPerGroup: 2,
      spaceBetween: 16,
      watchOverflow: true,
      preloadImages: false,
      speed: 1000,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
        pauseOnMouseEnter: true,
      },
      navigation: {
        nextEl: brandsContainer.querySelector('.swiper-button-next'),
        prevEl: brandsContainer.querySelector('.swiper-button-prev'),
      },
      pagination: {
        el: brandsContainer.querySelector('.swiper-pagination'),
        type: 'bullets',
        clickable: true,
      },
      breakpoints: {
        992: {
          slidesPerView: 8,
        },
        1368: {
          slidesPerView: 10,
        },
      }
    });

    brandsSwiper.autoplay.stop();

    const initAutoplay = () => {
      brandsSwiper.autoplay.start();
    }

    const eventType = (window.innerWidth <= 1024) ? 'scroll' : 'mousemove'
    window.addEventListener(eventType, initAutoplay, { once: true });
  }
}