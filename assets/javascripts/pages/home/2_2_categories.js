import Swiper from 'swiper';
import { Pagination } from 'swiper/modules';

export default function setBlogPosts() {
  const sections = document.querySelectorAll('[data-blog-posts]');

  sections.forEach((section) => {
    const swiperEl = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');

    const carousel = new Swiper(swiperEl, {
      modules: [Pagination],
      slidesPerView: 1.2,
      spaceBetween: 24,
      speed: 1000,
      watchOverflow: true,
      pagination: {
        el: pagination,
        type: 'bullets',
        clickable: true,
      },
      breakpoints: {
        768: { slidesPerView: 3 },
        1280: { slidesPerView: 4 },
      },
    });
  });
}
