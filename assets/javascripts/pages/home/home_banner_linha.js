import Swiper from 'swiper';
import { Pagination } from 'swiper/modules';

export default function setBannersLinha() {
  const sections = document.querySelectorAll('[data-banner-linha]');

  sections.forEach((section) => {
    const carousel = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');

    new Swiper(carousel, {
      modules: [Pagination],
      slidesPerView: 1.2,
      spaceBetween: 8,
      watchOverflow: true,
      speed: 1000,
      pagination: {
        el: pagination,
        type: 'bullets',
        clickable: true,
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
          spaceBetween: 24,
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 24,
        },
      },
    });
  });
}
