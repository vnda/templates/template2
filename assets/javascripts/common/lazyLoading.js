const LazyLoading = {
  threshold: window.innerWidth < 768 ? 350 : 600,
  nativeImageSupport: "loading" in HTMLImageElement.prototype,
  nativeIframeSupport: "loading" in HTMLIFrameElement.prototype,

  setForNativeLazy: function(element) {
    if (element.hasAttribute('src') || element.hasAttribute('srcset')) {
      // console.warn('Elemento já está com lazy load nativo configurado', element)
      return
    }

    if (element.hasAttribute('data-src')) {
      element.setAttribute('src', element.getAttribute('data-src'))
    }

    if (element.hasAttribute('data-srcset')) {
      element.setAttribute('srcset', element.getAttribute('data-srcset'))
    }
    
    if (element.hasAttribute('data-sizes')) {
      element.setAttribute('sizes', element.getAttribute('data-sizes'))
    }

    element.setAttribute('data-lazy-loaded', true)

    return true
  },

  update: function() {

    if (!this.nativeImageSupport || !this.nativeIframeSupport) {
      window.lazyLoadInstance.update()
    }

    return LazyLoading.init()
  },

  init: function() {
    const lazyImages = document.querySelectorAll("img.lazy");
    const lazyIframes = document.querySelectorAll('iframe.lazy')

    if (LazyLoading.nativeImageSupport) {
      lazyImages.forEach(image => {
        LazyLoading.setForNativeLazy(image)
      });
    }

    if (LazyLoading.nativeIframeSupport) {
      lazyIframes.forEach(iframe => {
        LazyLoading.setForNativeLazy(iframe)
      })
    }

    // Caso tenha o suporte em um, mas não tenha no outro
    if (LazyLoading.nativeImageSupport && !LazyLoading.nativeIframeSupport) {
      lazyImages.forEach(image => {
        image.classList.remove('lazy')
      })
    }

    if (!LazyLoading.nativeImageSupport && LazyLoading.nativeIframeSupport) {
      lazyIframes.forEach(iframe => {
        iframe.classList.remove('lazy')
      })
    }


    // Adiciona vanilla lazy load caso não tenha suporte nativo para algum dos elementos
    if (!LazyLoading.nativeImageSupport || !LazyLoading.nativeIframeSupport) {
      const script = document.createElement('script')
      script.setAttribute('async', true)
      script.src = '/javascripts/lazyload.min.js'
      document.body.appendChild(script)

      window.lazyLoadOptions = {
        elements_selector: ".lazy",
        threshold: LazyLoading.threshold
      };

      window.addEventListener("LazyLoad::Initialized", event => {
        window.lazyLoadInstance = event.detail.instance
      }, { passive: true });
    }

    return window.lazyLoad = LazyLoading
  }
}

export default LazyLoading
