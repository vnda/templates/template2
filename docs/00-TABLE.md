
<!-- _class: table-of-contents -->

# Orientações de cadastro

![Logo Vnda](../images/prints/vnda.svg)

## [Tabela de conteúdos](#1)

- ### [GERAL](#2)    - [1 POPUP DE NEWSLETTER](#2)    - [2 CARRINHO - FRETE GRÁTIS](#2)    - [3 SUGESTÕES CARRINHO](#2)    - [4 BANNER FAIXA TOPO](#2)    - [5 LOGO PRINCIPAL](#2)    - [6 MENU PRINCIPAL](#2)    - [7 BANNER DO SUBMENU](#2)    - [8 NEWSLETTER](#2)    - [9 MENU](#2)    - [10 REDES SOCIAIS](#2)    - [11 CNPJ](#2) - ### [HOME](#3)    - [1 FULLBANNER PRINCIPAL](#3)    - [2 BANNERS DE LINHA (até 3)](#3)    - [3 CARROSSEL DE PRODUTOS SUPERIOR](#3)    - [4 MARCAS - CARROSSEL](#3)    - [5 BANNER HORIZONTAL](#3)    - [6 ÍCONES DA HOME](#3) - ### [TAG](#4)    - [1 TAG FULLBANNER](#4)    - [2 SLIDER DE CATEGORIAS](#4)    - [3 FILTRO](#4)    - [4 TAG FLAG](#4) - ### [PRODUTO](#5)    - [1 IMAGENS](#5)    - [2 DESCRIÇÃO](#5)    - [3 VARIANTES](#5)    - [4 TAG MODELO](#5)    - [5 GUIA DE MEDIDAS](#5)    - [6 TAG BANNER](#5)    - [7 BANNERS DE PRODUTO](#5)    - [8 SEÇÃO COMPRE JUNTO (até 3 produtos)](#5)    - [9 PRODUTOS RELACIONADOS](#5) - ### [ATENDIMENTO](#6)    - [1 FULLBANNER TOPO](#6)    - [2 BANNER CONTEÚDO - TEXTO SUPERIOR](#6)    - [3 FORMULÁRIO DE CONTATO](#6)    - [4 TEXTO FORMULÁRIO](#6)    - [5 BANNER DE INFORMAÇÕES DA LOJA (até 4)](#6) - ### [SOBRE](#7)    - [1 FULLBANNER TOPO](#7)    - [2 BANNER CONTEÚDO - TEXTO SUPERIOR](#7)    - [3 BANNER IMAGEM E TEXTO](#7)    - [4 BANNER HORIZONTAL](#7)    - [5 BANNERS DEPOIMENTO](#7) - ### [TERMOS](#8)    - [1 FULLBANNER TOPO](#8)    - [2 ABAS LATERAIS](#8) - ### [ONDE-ENCONTRAR](#9)    - [1 FULLBANNER TOPO](#9)    - [2 LOCAIS](#9) 

***
