
- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/05-atendimento.png)

# ATENDIMENTO

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

**_Informações:_**

| Dúvida                       | Instrução                                                             |
| ---------------------------- | --------------------------------------------------------------------- |
| **Onde cadastrar**           | Páginas                                                               |
| **Onde será exibido**        | Página de atendimento da loja                                        |
| **Cadastro exemplo**         | [Admin](https://template2.vnda.dev/admin/paginas/editar?id=atendimento) |
| **Página para visualização** | [Página](https://template2.vnda.dev/p/atendimento)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?     | Orientação                                          |
| ------------- | -------------- | --------------------------------------------------- |
| **Titulo**    | :black_circle: | Titulo da página                                    |
| **Url**       | :black_circle: | "atendimento"                                      |
| **Descrição** | :black_circle: | Descrição da meta tag. Utilizada para melhorar SEO. |
| **Descrição** | :black_circle: | "."                                                 |

&nbsp;

## FULLBANNER TOPO

**_Informações:_**

| Dúvida                | Instrução                                        |
| --------------------- | ------------------------------------------------ |
| **Onde cadastrar**    | Banners                                          |
| **Onde será exibido** | Topo do layout padrão das páginas institucionais |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/)         |

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                          |
| ------------------- | ------------------- | ------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x364 pixels, Mobile: 1000x483 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                       |
| **Subtítulo**       | :large_blue_circle: | `Texto do botão \| Posição do texto`. Opções abaixo                   |
| **Descrição**       | :large_blue_circle: | Texto do banner em markdown. Exemplo abaixo                         |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba              |
| **URL**             | :large_blue_circle: | Link de direcionamento                                              |
| **Posição Desktop** | :black_circle:      | `atendimento-banner-principal`                              |
| **Posição Mobile**  | :black_circle:      | `atendimento-banner-principal-mobile`                       |
| **Cor**             | :large_blue_circle: | Cor do texto                                                        |

&nbsp;

**_Posições disponíveis:_**

- left
- center
- right

left: alinhado à esquerda
center: centralizado
right: alinhado à direita

**_Importante_**

Por padrão, a posição do texto é `center`. Para alterar a posição, sem utilizar botão, basta inserir `\| posição desejada`. Ex.: `\| left`

**_Exemplo Descrição do Banner:_**

```md
\#\#\# Upper title

\# Título do Banner

Breve descrição do banner.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).


&nbsp;

## BANNER CONTEÚDO - TEXTO SUPERIOR

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Banner texto a baixo do banner topo                           |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/admin/midias/editar?id=75) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                         |
| ------------- | ------------------- | ---------------------------------- |
| **Imagem**    | :no_entry:          |                                    |
| **Título**    | :black_circle:      | Controle interno                   |
| **Subtítulo** | :no_entry:          | Título da seção de texto principal |
| **Descrição** | :large_blue_circle: | Texto da seção, aceita markdown.   |
| **Externo?**  | :no_entry:          |                                    |
| **URL**       | :no_entry:          |                                    |
| **Posição**   | :black_circle:      | `atendimento-banner-conteudo`     |
| **Cor**       | :no_entry:          |                                    |


&nbsp;


## FORMULÁRIO DE CONTATO

- Criar formulário no [Admin](https://template2.vnda.dev/admin/config/mensagens-e-avisos/forms).
- Com o assunto `contato`.

&nbsp;

## TEXTO FORMULÁRIO

**_Informações:_**

| Dúvida                          | Instrução                                                |
| ------------------------------- | -------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                  |
| **Onde será exibido**           | Acima do formulário de contato                           |
| **Cadastro exemplo em staging** | [Admin](https://template2.vnda.dev/) |

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação               |
| ------------- | ------------------- | ------------------------ |
| **Imagem**    | :no_entry:          |                          |
| **Título**    | :black_circle:      | Controle interno         |
| **Subtítulo** | :large_blue_circle: | Título do formulário     |
| **Descrição** | :large_blue_circle: | Texto do formulário      |
| **Externo?**  | :no_entry:          |                          |
| **URL**       | :no_entry:          |                          |
| **Posição**   | :black_circle:      | `atendimento-form-texto` |
| **Cor**       | :no_entry:          |                          |

&nbsp;

## BANNER DE INFORMAÇÕES DA LOJA (até 4)

**_Informações:_**

| Dúvida                          | Instrução                                                |
| ------------------------------- | -------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                  |
| **Onde será exibido**           | Abaixo do texto do formulário                            |
| **Cadastro exemplo em staging** | [Admin](https://template2.vnda.dev/) |

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                     |
| ------------- | ------------------- | ------------------------------ |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 24x24 pixels |
| **Título**    | :black_circle:      | Alt da imagem                  |
| **Subtítulo** | :no_entry:          |                                |
| **Descrição** | :large_blue_circle: | Texto curto da seção           |
| **Externo?**  | :no_entry:          |                                |
| **URL**       | :no_entry:          |                                |
| **Posição**   | :black_circle:      | `atendimento-infos-loja`       |
| **Cor**       | :no_entry:          |                                |

***
