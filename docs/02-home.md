- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/02-home.png)

# HOME

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |


&nbsp;

## FULLBANNER PRINCIPAL

**_Informações:_**

| Dúvida                | Instrução                                                        |
| --------------------- | ---------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                          |
| **Onde será exibido** | Banner principal abaixo do header, ocupa 100% da largura da tela |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/admin/midias/editar?id=1) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                            |
| ------------------- | ------------------- | --------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x1012 pixels, Mobile: 1000x1460 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                         |
| **Subtítulo**       | :large_blue_circle: | Título do botão e posição do texto do banner. Opções a baixo!         |
| **Descrição**       | :large_blue_circle: | Título e descrição do banner                                          |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba                |
| **URL**             | :large_blue_circle: | Link de direcionamento                                                |
| **Posição Desktop** | :black_circle:      | `home-banner-principal`                                        |
| **Posição Mobile**  | :black_circle:      | `home-banner-principal-mobile`                                 |
| **Cor**             | :large_blue_circle: |  Cor dos textos                                                       |

&nbsp;

**_Exemplo de subtítulo:_**

```md
CALL TO ACTION | left-center
```

**_Exemplo de descrição:_**

```md
\#\#\# UPPER TITLE
\#\# Título do Banner

Descrição do banner alinhado à esquerda.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

**_Posições do botão possíveis para colocar no campo subtítulo:_**

- left-top
- left-center
- left-bottom

- center-top
- center-center
- center-bottom

- right-top
- right-center
- right-bottom


&nbsp;

## BANNERS DE LINHA (até 3)

***Informações:***

| Dúvida                          | Instrução                                                    |
| :------------------------------ | :----------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                      |
| **Onde será exibido**           | Seção de banners linha, abaixo dos ícones                    |
| **Cadastro exemplo**            | [Admin](https://template2.vnda.dev)                      |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                                |
| ------------- | ------------------- | --------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 1000x1250 pixels                        |
| **Título**    | :black_circle:      | Apenas no alt das imagens. Não exibido no front           |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se uma url for cadastrada |
| **Descrição** | :large_blue_circle: | Texto do banner. Exemplo abaixo                           |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba    |
| **URL**       | :large_blue_circle: | Link de direcionamento                                    |
| **Posição**   | :black_circle:      | `home-banner-linha`                                |
| **Cor**      | :no_entry:           |                                                           |

**_Exemplo de descrição:_**

```md
\#\# Categoria

Breve texto descritivo do banner em até duas linhas e centralizado.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

## CARROSSEL DE PRODUTOS SUPERIOR

**_Informações:_**

| Dúvida                | Instrução                                                                       |
| --------------------- | ------------------------------------------------------------------------------- |
| **Onde cadastrar**    | Tags                                                                            |
| **Onde será exibido** | Carrossel de produtos, abaixo das tabs de categoria                             |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/admin/tags/editar?id=home-produtos) |

&nbsp;

**_Informações sobre os campos_**

| Campo         | Funcional?          | Orientação                                   |
| ------------- | ------------------- | -------------------------------------------- |
| **Nome**      | :black_circle:      | `home-produtos`                       |
| **Título**    | :large_blue_circle: | Título do carrossel                          |
| **Subtítulo** | :no_entry:          |                                              |
| **Descrição** | :no_entry:          |                                              |
| **Tipo**      | :no_entry:          |                                              |
| **Imagem**    | :no_entry:          |                                              |

## MARCAS - CARROSSEL

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                             |
| ------------- | ------------------- | ------------------------------------------------------ |
| **Imagem**    | :black_circle:      | Dimensões sugeridas 200 pixels de largura x altura livre. Importante não ultrapassar de 200x400 pixels |
| **Título**    | :black_circle:      | Alt da imagem                                          |
| **Subtítulo** | :no_entry:          |                                                        |
| **Descrição** | :no_entry:          |                                                        |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba |
| **URL**       | :large_blue_circle: | Link de direcionamento                                 |
| **Posição**   | :black_circle:      | `home-marcas`                                          |
| **Cor**       | :no_entry:          |                                                        |


&nbsp;
&nbsp;

## BANNER HORIZONTAL

***Informações:***

| Dúvida                          | Instrução                                                           |
| :------------------------------ | :------------------------------------------------------------------ |
| **Onde cadastrar**              | Banners                                                             |
| **Onde será exibido**           | Seção com um banner horizontal que fica abaixo dos banners de apoio |
| **Cadastro exemplo em staging** | [Admin](https://template2.vnda.dev/admin/midias/editar?id=6)    |

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                           |
| ------------- | ------------------- | ---------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensões sugeridas: 1400x350 pixels                 |
| **Título**    | :black_circle:      | Apenas no alt das imagens. Não exibido no front      |
| **Subtítulo** | :large_blue_circle: | Texto do botão e posição dos textos. Opções a baixo! |
| **Descrição** | :large_blue_circle: | Colocar descrição do banner. Aceita markdown.        |
| **Externo?**  | :large_blue_circle: |                                                      |
| **URL**       | :large_blue_circle: | Colocar link de click no banner                      |
| **Posição**   | :black_circle:      | `home-banner-horizontal`                      |
| **Cor**       | :large_blue_circle: | Colocar cor do texto                                 |

&nbsp;

**_Exemplo de subtítulo:_**

```md
Saiba mais | center
```

**_Exemplo de descrição:_**

```md
\#\#\# Upper Title
\#\# Título principal

Lorem ipsum dolor sit amet, consectetur adipiscing elit
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

## ÍCONES DA HOME

***Informações:***

| Dúvida                          | Instrução                                                    |
| :------------------------------ | :----------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                      |
| **Onde será exibido**           | Seção de carrossel de ícons, abaixo do fullbanner            |
| **Cadastro exemplo em staging** | [Admin](https://template2.vnda.dev/admin/midias/editar?id=4) |

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                      |
| ------------- | ------------------- | ----------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 50x50 pixels                 |
| **Título**    | :black_circle:      | Apenas no alt das imagens. Não exibido no front |
| **Subtítulo** | :large_blue_circle: | Título do ícone                                 |
| **Descrição** | :large_blue_circle: | Descrição curta do ícone                        |
| **Externo?**  | :large_blue_circle: |  Selecionar se o link do banner deve abrir em outra aba|
| **URL**       | :large_blue_circle: |  Link de direcionamento                         |
| **Posição**   | :black_circle:      | `home-icones-vertical`                   |
| **Cor**       | :no_entry:          |                                                 |


***
