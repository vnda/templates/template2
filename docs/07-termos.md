
- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/07-termos.png)

# TERMOS

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

**_Informações:_**

| Dúvida                       | Instrução                                                             |
| ---------------------------- | --------------------------------------------------------------------- |
| **Onde cadastrar**           | Páginas                                                               |
| **Onde será exibido**        | Página de termos da loja                                        |
| **Cadastro exemplo**         | [Admin](https://template2.vnda.dev/admin/paginas/editar?id=termos) |
| **Página para visualização** | [Página](https://template2.vnda.dev/p/termos)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?     | Orientação                                          |
| ------------- | -------------- | --------------------------------------------------- |
| **Titulo**    | :black_circle: | Titulo da página                                    |
| **Url**       | :black_circle: | "termos"                                      |
| **Descrição** | :black_circle: | Descrição da meta tag. Utilizada para melhorar SEO. |
| **Descrição** | :black_circle: | "."                                                 |

&nbsp;

## FULLBANNER TOPO

**_Informações:_**

| Dúvida                | Instrução                                        |
| --------------------- | ------------------------------------------------ |
| **Onde cadastrar**    | Banners                                          |
| **Onde será exibido** | Topo do layout padrão das páginas institucionais |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/)         |

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                          |
| ------------------- | ------------------- | ------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x364 pixels, Mobile: 1000x483 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                       |
| **Subtítulo**       | :large_blue_circle: | `Texto do botão \| Posição do texto`. Opções abaixo                   |
| **Descrição**       | :large_blue_circle: | Texto do banner em markdown. Exemplo abaixo                         |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba              |
| **URL**             | :large_blue_circle: | Link de direcionamento                                              |
| **Posição Desktop** | :black_circle:      | `termos-banner-principal`                              |
| **Posição Mobile**  | :black_circle:      | `termos-banner-principal-mobile`                       |
| **Cor**             | :large_blue_circle: | Cor do texto                                                        |

&nbsp;

**_Posições disponíveis:_**

- left
- center
- right

left: alinhado à esquerda
center: centralizado
right: alinhado à direita

**_Importante_**

Por padrão, a posição do texto é `center`. Para alterar a posição, sem utilizar botão, basta inserir `\| posição desejada`. Ex.: `\| left`

**_Exemplo Descrição do Banner:_**

```md
\#\#\# Upper title

\# Título do Banner

Breve descrição do banner.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).


&nbsp;

## ABAS LATERAIS

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Seção de abas laterais                                        |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                          |
| ------------- | ------------------- | ----------------------------------- |
| **Imagem**    | :no_entry:          |                                     |
| **Título**    | :black_circle:      | Título do banner. Apenas para controle interno                       |
| **Subtítulo** | :black_circle:      | Título da aba                       |
| **Descrição** | :black_circle:      | Conteúdo da aba. Aceita Markdown    |
| **Externo?**  | :no_entry:          |                                     |
| **URL**       | :large_blue_circle: | Id da aba. Ex.: "termos-de-uso", "politicas-dado". O id servirá para vincular o botão ao conteúdo textual e também como link para menus em rodapé etc |
| **Posição**   | :black_circle:      | `termos-abas`                 |
| **Cor**       | :no_entry:          |                                     |

***
