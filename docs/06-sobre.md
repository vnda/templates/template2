
- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/06-sobre.png)

# SOBRE

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

**_Informações:_**

| Dúvida                       | Instrução                                                             |
| ---------------------------- | --------------------------------------------------------------------- |
| **Onde cadastrar**           | Páginas                                                               |
| **Onde será exibido**        | Página de sobre da loja                                        |
| **Cadastro exemplo**         | [Admin](https://template2.vnda.dev/admin/paginas/editar?id=sobre) |
| **Página para visualização** | [Página](https://template2.vnda.dev/p/sobre)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?     | Orientação                                          |
| ------------- | -------------- | --------------------------------------------------- |
| **Titulo**    | :black_circle: | Titulo da página                                    |
| **Url**       | :black_circle: | "sobre"                                      |
| **Descrição** | :black_circle: | Descrição da meta tag. Utilizada para melhorar SEO. |
| **Descrição** | :black_circle: | "."                                                 |

&nbsp;

## FULLBANNER TOPO

**_Informações:_**

| Dúvida                | Instrução                                        |
| --------------------- | ------------------------------------------------ |
| **Onde cadastrar**    | Banners                                          |
| **Onde será exibido** | Topo do layout padrão das páginas institucionais |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/)         |

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                          |
| ------------------- | ------------------- | ------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x364 pixels, Mobile: 1000x483 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                       |
| **Subtítulo**       | :large_blue_circle: | `Texto do botão \| Posição do texto`. Opções abaixo                   |
| **Descrição**       | :large_blue_circle: | Texto do banner em markdown. Exemplo abaixo                         |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba              |
| **URL**             | :large_blue_circle: | Link de direcionamento                                              |
| **Posição Desktop** | :black_circle:      | `sobre-banner-principal`                              |
| **Posição Mobile**  | :black_circle:      | `sobre-banner-principal-mobile`                       |
| **Cor**             | :large_blue_circle: | Cor do texto                                                        |

&nbsp;

**_Posições disponíveis:_**

- left
- center
- right

left: alinhado à esquerda
center: centralizado
right: alinhado à direita

**_Importante_**

Por padrão, a posição do texto é `center`. Para alterar a posição, sem utilizar botão, basta inserir `\| posição desejada`. Ex.: `\| left`

**_Exemplo Descrição do Banner:_**

```md
\#\#\# Upper title

\# Título do Banner

Breve descrição do banner.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).


&nbsp;

## BANNER CONTEÚDO - TEXTO SUPERIOR

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Banner texto a baixo do banner topo                           |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/admin/midias/editar?id=75) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                         |
| ------------- | ------------------- | ---------------------------------- |
| **Imagem**    | :no_entry:          |                                    |
| **Título**    | :black_circle:      | Controle interno                   |
| **Subtítulo** | :no_entry:          | Título da seção de texto principal |
| **Descrição** | :large_blue_circle: | Texto da seção, aceita markdown.   |
| **Externo?**  | :no_entry:          |                                    |
| **URL**       | :no_entry:          |                                    |
| **Posição**   | :black_circle:      | `sobre-banner-conteudo`     |
| **Cor**       | :no_entry:          |                                    |


&nbsp;

## BANNER IMAGEM E TEXTO

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Banner texto e imagem                                         |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                       |
| ------------- | ------------------- | -------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 1000x625 pixels|
| **Título**    | :black_circle:      | Alt da imagem                    |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se uma url for cadastrada |
| **Descrição** | :large_blue_circle: | Texto do banner. Exemplo abaixo                           |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba    |
| **URL**       | :large_blue_circle: | Link de direcionamento                                    |
| **Posição**   | :black_circle:      | `sobre-banner-destaque`|
| **Cor**       | :no_entry:          |                                  |

**_Exemplo de descrição:_**

```md
\#\# TÍTULO DA SEÇÃO

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
```

**OBSERVAÇÕES**:

- É possível utilizar somente texto ou somente imagem. Basta não cadastrar o conteúdo que não deseja exibir.
- Obrigatório remover as barras antes dos jogos da velha (hashtag).
## BANNER HORIZONTAL

**_Informações:_**

| Dúvida                | Instrução                                        |
| --------------------- | ------------------------------------------------ |
| **Onde cadastrar**    | Banners                                          |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/)         |

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                          |
| ------------------- | ------------------- | ------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x573 pixels, Mobile: 1000x796 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                       |
| **Subtítulo**       | :large_blue_circle: | Texto do botão \| Posição do texto. Opções abaixo                   |
| **Descrição**       | :large_blue_circle: | Texto do banner em markdown. Exemplo abaixo                         |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba              |
| **URL**             | :large_blue_circle: | Link de direcionamento                                              |
| **Posição Desktop** | :black_circle:      | `sobre-banner-horizontal`                             |
| **Posição Mobile**  | :black_circle:      | `sobre-banner-horizontal-mobile`                      |
| **Cor**             | :large_blue_circle: | Cor do texto                                                        |

&nbsp;

**_Posições disponíveis:_**

- left
- center
- right

left: alinhado à esquerda
center: centralizado
right: alinhado à direita

**_Importante_**

Por padrão, a posição do texto é `center`. Para alterar a posição, sem utilizar botão, basta inserir "\| posição desejada". Ex.: "\| left"

**_Exemplo Descrição do Banner:_**

```md
\#\#\# Upper title

\# Título do Banner

Breve descrição do banner.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).
&nbsp;

## BANNERS DEPOIMENTO

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Cadastro exemplo**  | [Admin](https://template2.vnda.dev/)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                           |
| ------------- | ------------------- | ------------------------------------ |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 320x320 pixels     |
| **Título**    | :black_circle:      | Alt da imagem                        |
| **Subtítulo** | :large_blue_circle: | Título e subtítulo. Exemplo a baixo. |
| **Descrição** | :large_blue_circle: | Breve descrição                      |
| **Externo?**  | :no_entry:          |                                      |
| **URL**       | :no_entry:          |                                      |
| **Posição**   | :black_circle:      | `sobre-banner-depoimentos`|
| **Cor**       | :no_entry:          |                                      |

&nbsp;

**_Exemplo Subtítulo do Banner:_**

```md
Título | Subtítulo
```

***
